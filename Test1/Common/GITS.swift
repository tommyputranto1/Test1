//
//  GITS.swift
//  Test1
//
//  Created by GITS  on 1/23/17.
//  Copyright © 2017 GITS . All rights reserved.
//


import Foundation
import Alamofire
import SwiftyJSON

typealias SuccessResponse = (JSON?) -> Void
typealias ErrorResponse  = (String) -> Void

class GITS {
    static let instance = GITS()
    
    init(){
    }
    
    func request(request:URLRequestConvertible,onSuccess:@escaping SuccessResponse,onError:@escaping ErrorResponse){
        Alamofire.request(request).responseJSON { (value) in
            if value.result.error == nil {
                let json = JSON(value.result.value!)
                print("GITS \(json)")
                let code = json["msg"].stringValue
                let result = json["result"].stringValue
                if code == "success" || result == "OK" || json == json {
                    onSuccess(json)
                }else{
                    onError(code)
                }
            }else{
                let em = "\(value.result.error)"
                if em.lowercased().range(of: "offline") != nil{
                    onError("No internet connection")
                }else{
                    onError("There was an error in our system, please wait a moment")
                }
            }
        }
    }
    
}
