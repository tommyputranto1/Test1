//
//  ApiMain.swift
//  Test1
//
//  Created by GITS  on 1/23/17.
//  Copyright © 2017 GITS . All rights reserved.
//


import Foundation
import Alamofire

enum ApiMain: URLRequestConvertible {
    
    case list()
    
    var res : (path:String,param:Parameters) {
        switch self {
        case .list():
            return ("shopping",["callname" : "FindPopularItems", "responseencoding":"JSON","appid":"FandyGot-ec13-4906-b11c-94d9e36684ee","siteid":"0","QueryKeywords":"dog","version":"713" ])
        }
        
    }
    
    var method:HTTPMethod{
        return HTTPMethod.get
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstant.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(res.path))
        urlRequest.httpMethod = method.rawValue
        print("URI : \(urlRequest.url?.absoluteString)" )
        print("PARAM : \(res.param)")
        return try URLEncoding.default.encode(urlRequest, with: res.param)
    }
}
