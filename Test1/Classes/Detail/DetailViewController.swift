//
//  DetailViewController.swift
//  Test1
//
//  Created by GITS  on 1/24/17.
//  Copyright © 2017 GITS . All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIWebViewDelegate {
    var url : String?
    @IBOutlet var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        if let urls = URL(string: url!) {
            let request = URLRequest(url: urls)
            webView.loadRequest(request)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
