//
//  MainViewController.swift
//  Test1
//
//  Created by GITS  on 1/23/17.
//  Copyright © 2017 GITS . All rights reserved.
//

import UIKit
import Kingfisher

class MainViewController: UIViewController {
    var input : MainRequest?
    var imageData : [String] = []
    var titleData : [String] = []
    var urlData : [String] = []
    var url : String?
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.tag = 2
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops
        collectionView.reloadData()
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.sizeToFit()
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
        layout.minimumInteritemSpacing = 10;
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/5,height: 160)

        input?.dogetData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
extension MainViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let image = cell.viewWithTag(10) as! UIImageView
        let title = cell.viewWithTag(11) as! UILabel
        let urlimage = URL(string : imageData[indexPath.row])
        image.kf.setImage(with: urlimage)
        title.text = "\(titleData[indexPath.row])"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        url = urlData[indexPath.row]
        self.performSegue(withIdentifier: "segue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc : DetailViewController = segue.destination as! DetailViewController
        vc.url = url
    }
}
extension MainViewController : MainResponse{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configMain(vc: self)
    }
    func configMain (vc:MainViewController){
        let request = MainPresent()
        request.output = vc
        vc.input = request
    }
    func displayData(images : [String] , titles : [String], urls : [String]) {
        imageData = images
        titleData = titles
        urlData = urls
        collectionView.reloadData()
    }
    
}
