//
//  WorkerPresent.swift
//  Test1
//
//  Created by GITS  on 1/23/17.
//  Copyright © 2017 GITS . All rights reserved.
//

import Foundation

class WorkerPresent {
    class func GetData(res:MainResponse? ){
        GITS.instance.request(request: ApiMain.list(), onSuccess: { (JSON) in
            let json = GBaseClass(JSONString: (JSON?.rawString())!)
            let dataItems = (json?.itemArray?.item)!
            var titles : [String] = []
            var images : [String] = []
            var urls : [String] = []
            for data in dataItems{
                titles.append(data.title!)
                if data.galleryURL != nil{
                      images.append(data.galleryURL!)
                }else{
                    images.append("")
                }
                if data.viewItemURLForNaturalSearch != nil{
                    urls.append(data.viewItemURLForNaturalSearch!)
                }else{
                    urls.append("")
                }
            }
            res?.displayData(images: images, titles: titles, urls : urls)
            }) { (error) in
                
        }
//        GITS.instance.request(request: ApiArticle.getDetailArticle(id: id), onSuccess: { (json) in
//            var title: String?
//            var author: String?
//            var date: String?
//            var image: String?
//            var description: String?
//            var total : [String] = []
//            var listRelated :[DARelated] = []
//            var listComment: [DARows] = []
//            var totalCom : Int?
//            let json = DABaseClass(JSONString: (json?.rawString())!)
//            let data = json?.detailartikel
//            let related = data?.related
//            let totalComment = json?.comments?.total
//            listRelated = related!
//            listComment = (json?.comments?.rows)!
//            if listComment.count > 3 {
//                totalCom = listComment.count
//            }else{
//                totalCom = 0
//            }
//            title = data?.title
//            author = data?.author
//            date = data?.artikeldate
//            image = data?.urlimg
//            description = data?.desc
//            total = ["id"]
//            res?.displayCommentArticle(comment: listComment, total : totalCom!)
//            res?.displayDetailArticle(title: title!, author: author!, date: date!, image: image!, desc: description!, total: total, totalComment: totalComment!)
//            res?.displayRelatedArticle(related: listRelated)
//            
//        }) { (error) in
//            print("error \(error)")
//            res?.displayError(data: error, param: [:])
//        }
    }

}
