//
//  MainPresent.swift
//  Test1
//
//  Created by GITS  on 1/23/17.
//  Copyright © 2017 GITS . All rights reserved.
//

import Foundation
protocol MainRequest{
    func dogetData()
}

protocol MainResponse {
    func displayData(images : [String] , titles : [String], urls : [String])
}

class MainPresent: MainRequest{
  
    var output:MainResponse?
    
    internal func dogetData() {
        WorkerPresent.GetData(res: output)
    }
}
