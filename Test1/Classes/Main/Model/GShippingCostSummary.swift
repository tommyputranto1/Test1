//
//  GShippingCostSummary.swift
//
//  Created by GITS  on 1/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class GShippingCostSummary: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kGShippingCostSummaryListedShippingServiceCostKey: String = "ListedShippingServiceCost"
  private let kGShippingCostSummaryShippingServiceCostKey: String = "ShippingServiceCost"
  private let kGShippingCostSummaryShippingTypeKey: String = "ShippingType"

  // MARK: Properties
  public var listedShippingServiceCost: GListedShippingServiceCost?
  public var shippingServiceCost: GShippingServiceCost?
  public var shippingType: String?

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    listedShippingServiceCost <- map[kGShippingCostSummaryListedShippingServiceCostKey]
    shippingServiceCost <- map[kGShippingCostSummaryShippingServiceCostKey]
    shippingType <- map[kGShippingCostSummaryShippingTypeKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = listedShippingServiceCost { dictionary[kGShippingCostSummaryListedShippingServiceCostKey] = value.dictionaryRepresentation() }
    if let value = shippingServiceCost { dictionary[kGShippingCostSummaryShippingServiceCostKey] = value.dictionaryRepresentation() }
    if let value = shippingType { dictionary[kGShippingCostSummaryShippingTypeKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.listedShippingServiceCost = aDecoder.decodeObject(forKey: kGShippingCostSummaryListedShippingServiceCostKey) as? GListedShippingServiceCost
    self.shippingServiceCost = aDecoder.decodeObject(forKey: kGShippingCostSummaryShippingServiceCostKey) as? GShippingServiceCost
    self.shippingType = aDecoder.decodeObject(forKey: kGShippingCostSummaryShippingTypeKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(listedShippingServiceCost, forKey: kGShippingCostSummaryListedShippingServiceCostKey)
    aCoder.encode(shippingServiceCost, forKey: kGShippingCostSummaryShippingServiceCostKey)
    aCoder.encode(shippingType, forKey: kGShippingCostSummaryShippingTypeKey)
  }

}
