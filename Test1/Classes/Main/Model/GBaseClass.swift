//
//  GBaseClass.swift
//
//  Created by GITS  on 1/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class GBaseClass: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kGBaseClassTimestampKey: String = "Timestamp"
  private let kGBaseClassItemArrayKey: String = "ItemArray"
  private let kGBaseClassBuildKey: String = "Build"
  private let kGBaseClassAckKey: String = "Ack"
  private let kGBaseClassVersionKey: String = "Version"

  // MARK: Properties
  public var timestamp: String?
  public var itemArray: GItemArray?
  public var build: String?
  public var ack: String?
  public var version: String?

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    timestamp <- map[kGBaseClassTimestampKey]
    itemArray <- map[kGBaseClassItemArrayKey]
    build <- map[kGBaseClassBuildKey]
    ack <- map[kGBaseClassAckKey]
    version <- map[kGBaseClassVersionKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = timestamp { dictionary[kGBaseClassTimestampKey] = value }
    if let value = itemArray { dictionary[kGBaseClassItemArrayKey] = value.dictionaryRepresentation() }
    if let value = build { dictionary[kGBaseClassBuildKey] = value }
    if let value = ack { dictionary[kGBaseClassAckKey] = value }
    if let value = version { dictionary[kGBaseClassVersionKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.timestamp = aDecoder.decodeObject(forKey: kGBaseClassTimestampKey) as? String
    self.itemArray = aDecoder.decodeObject(forKey: kGBaseClassItemArrayKey) as? GItemArray
    self.build = aDecoder.decodeObject(forKey: kGBaseClassBuildKey) as? String
    self.ack = aDecoder.decodeObject(forKey: kGBaseClassAckKey) as? String
    self.version = aDecoder.decodeObject(forKey: kGBaseClassVersionKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(timestamp, forKey: kGBaseClassTimestampKey)
    aCoder.encode(itemArray, forKey: kGBaseClassItemArrayKey)
    aCoder.encode(build, forKey: kGBaseClassBuildKey)
    aCoder.encode(ack, forKey: kGBaseClassAckKey)
    aCoder.encode(version, forKey: kGBaseClassVersionKey)
  }

}
