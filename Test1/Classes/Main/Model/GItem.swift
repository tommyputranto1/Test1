//
//  GItem.swift
//
//  Created by GITS  on 1/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class GItem: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kGItemTitleKey: String = "Title"
  private let kGItemGalleryURLKey: String = "GalleryURL"
  private let kGItemShippingCostSummaryKey: String = "ShippingCostSummary"
  private let kGItemTimeLeftKey: String = "TimeLeft"
  private let kGItemItemIDKey: String = "ItemID"
  private let kGItemViewItemURLForNaturalSearchKey: String = "ViewItemURLForNaturalSearch"
  private let kGItemWatchCountKey: String = "WatchCount"
  private let kGItemPrimaryCategoryIDKey: String = "PrimaryCategoryID"
  private let kGItemListingTypeKey: String = "ListingType"
  private let kGItemConvertedCurrentPriceKey: String = "ConvertedCurrentPrice"
  private let kGItemPrimaryCategoryNameKey: String = "PrimaryCategoryName"
  private let kGItemEndTimeKey: String = "EndTime"
  private let kGItemBidCountKey: String = "BidCount"
  private let kGItemListingStatusKey: String = "ListingStatus"
  private let kGItemDiscountPriceInfoKey: String = "DiscountPriceInfo"

  // MARK: Properties
  public var title: String?
  public var galleryURL: String?
  public var shippingCostSummary: GShippingCostSummary?
  public var timeLeft: String?
  public var itemID: String?
  public var viewItemURLForNaturalSearch: String?
  public var watchCount: Int?
  public var primaryCategoryID: String?
  public var listingType: String?
  public var convertedCurrentPrice: GConvertedCurrentPrice?
  public var primaryCategoryName: String?
  public var endTime: String?
  public var bidCount: Int?
  public var listingStatus: String?
  public var discountPriceInfo: GDiscountPriceInfo?

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    title <- map[kGItemTitleKey]
    galleryURL <- map[kGItemGalleryURLKey]
    shippingCostSummary <- map[kGItemShippingCostSummaryKey]
    timeLeft <- map[kGItemTimeLeftKey]
    itemID <- map[kGItemItemIDKey]
    viewItemURLForNaturalSearch <- map[kGItemViewItemURLForNaturalSearchKey]
    watchCount <- map[kGItemWatchCountKey]
    primaryCategoryID <- map[kGItemPrimaryCategoryIDKey]
    listingType <- map[kGItemListingTypeKey]
    convertedCurrentPrice <- map[kGItemConvertedCurrentPriceKey]
    primaryCategoryName <- map[kGItemPrimaryCategoryNameKey]
    endTime <- map[kGItemEndTimeKey]
    bidCount <- map[kGItemBidCountKey]
    listingStatus <- map[kGItemListingStatusKey]
    discountPriceInfo <- map[kGItemDiscountPriceInfoKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = title { dictionary[kGItemTitleKey] = value }
    if let value = galleryURL { dictionary[kGItemGalleryURLKey] = value }
    if let value = shippingCostSummary { dictionary[kGItemShippingCostSummaryKey] = value.dictionaryRepresentation() }
    if let value = timeLeft { dictionary[kGItemTimeLeftKey] = value }
    if let value = itemID { dictionary[kGItemItemIDKey] = value }
    if let value = viewItemURLForNaturalSearch { dictionary[kGItemViewItemURLForNaturalSearchKey] = value }
    if let value = watchCount { dictionary[kGItemWatchCountKey] = value }
    if let value = primaryCategoryID { dictionary[kGItemPrimaryCategoryIDKey] = value }
    if let value = listingType { dictionary[kGItemListingTypeKey] = value }
    if let value = convertedCurrentPrice { dictionary[kGItemConvertedCurrentPriceKey] = value.dictionaryRepresentation() }
    if let value = primaryCategoryName { dictionary[kGItemPrimaryCategoryNameKey] = value }
    if let value = endTime { dictionary[kGItemEndTimeKey] = value }
    if let value = bidCount { dictionary[kGItemBidCountKey] = value }
    if let value = listingStatus { dictionary[kGItemListingStatusKey] = value }
    if let value = discountPriceInfo { dictionary[kGItemDiscountPriceInfoKey] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.title = aDecoder.decodeObject(forKey: kGItemTitleKey) as? String
    self.galleryURL = aDecoder.decodeObject(forKey: kGItemGalleryURLKey) as? String
    self.shippingCostSummary = aDecoder.decodeObject(forKey: kGItemShippingCostSummaryKey) as? GShippingCostSummary
    self.timeLeft = aDecoder.decodeObject(forKey: kGItemTimeLeftKey) as? String
    self.itemID = aDecoder.decodeObject(forKey: kGItemItemIDKey) as? String
    self.viewItemURLForNaturalSearch = aDecoder.decodeObject(forKey: kGItemViewItemURLForNaturalSearchKey) as? String
    self.watchCount = aDecoder.decodeObject(forKey: kGItemWatchCountKey) as? Int
    self.primaryCategoryID = aDecoder.decodeObject(forKey: kGItemPrimaryCategoryIDKey) as? String
    self.listingType = aDecoder.decodeObject(forKey: kGItemListingTypeKey) as? String
    self.convertedCurrentPrice = aDecoder.decodeObject(forKey: kGItemConvertedCurrentPriceKey) as? GConvertedCurrentPrice
    self.primaryCategoryName = aDecoder.decodeObject(forKey: kGItemPrimaryCategoryNameKey) as? String
    self.endTime = aDecoder.decodeObject(forKey: kGItemEndTimeKey) as? String
    self.bidCount = aDecoder.decodeObject(forKey: kGItemBidCountKey) as? Int
    self.listingStatus = aDecoder.decodeObject(forKey: kGItemListingStatusKey) as? String
    self.discountPriceInfo = aDecoder.decodeObject(forKey: kGItemDiscountPriceInfoKey) as? GDiscountPriceInfo
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(title, forKey: kGItemTitleKey)
    aCoder.encode(galleryURL, forKey: kGItemGalleryURLKey)
    aCoder.encode(shippingCostSummary, forKey: kGItemShippingCostSummaryKey)
    aCoder.encode(timeLeft, forKey: kGItemTimeLeftKey)
    aCoder.encode(itemID, forKey: kGItemItemIDKey)
    aCoder.encode(viewItemURLForNaturalSearch, forKey: kGItemViewItemURLForNaturalSearchKey)
    aCoder.encode(watchCount, forKey: kGItemWatchCountKey)
    aCoder.encode(primaryCategoryID, forKey: kGItemPrimaryCategoryIDKey)
    aCoder.encode(listingType, forKey: kGItemListingTypeKey)
    aCoder.encode(convertedCurrentPrice, forKey: kGItemConvertedCurrentPriceKey)
    aCoder.encode(primaryCategoryName, forKey: kGItemPrimaryCategoryNameKey)
    aCoder.encode(endTime, forKey: kGItemEndTimeKey)
    aCoder.encode(bidCount, forKey: kGItemBidCountKey)
    aCoder.encode(listingStatus, forKey: kGItemListingStatusKey)
    aCoder.encode(discountPriceInfo, forKey: kGItemDiscountPriceInfoKey)
  }

}
