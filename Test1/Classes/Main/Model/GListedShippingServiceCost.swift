//
//  GListedShippingServiceCost.swift
//
//  Created by GITS  on 1/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class GListedShippingServiceCost: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kGListedShippingServiceCostValueKey: String = "Value"
  private let kGListedShippingServiceCostCurrencyIDKey: String = "CurrencyID"

  // MARK: Properties
  public var value: Float?
  public var currencyID: String?

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    value <- map[kGListedShippingServiceCostValueKey]
    currencyID <- map[kGListedShippingServiceCostCurrencyIDKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = value { dictionary[kGListedShippingServiceCostValueKey] = value }
    if let value = currencyID { dictionary[kGListedShippingServiceCostCurrencyIDKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.value = aDecoder.decodeObject(forKey: kGListedShippingServiceCostValueKey) as? Float
    self.currencyID = aDecoder.decodeObject(forKey: kGListedShippingServiceCostCurrencyIDKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(value, forKey: kGListedShippingServiceCostValueKey)
    aCoder.encode(currencyID, forKey: kGListedShippingServiceCostCurrencyIDKey)
  }

}
