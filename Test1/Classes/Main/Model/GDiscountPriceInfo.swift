//
//  GDiscountPriceInfo.swift
//
//  Created by GITS  on 1/24/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public class GDiscountPriceInfo: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kGDiscountPriceInfoOriginalRetailPriceKey: String = "OriginalRetailPrice"
  private let kGDiscountPriceInfoPricingTreatmentKey: String = "PricingTreatment"
  private let kGDiscountPriceInfoSoldOneBayKey: String = "SoldOneBay"
  private let kGDiscountPriceInfoSoldOffeBayKey: String = "SoldOffeBay"

  // MARK: Properties
  public var originalRetailPrice: GOriginalRetailPrice?
  public var pricingTreatment: String?
  public var soldOneBay: Bool = false
  public var soldOffeBay: Bool = false

  // MARK: ObjectMapper Initalizers
  /**
   Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  required public init?(map: Map){

  }

  /**
  Map a JSON object to this class using ObjectMapper
   - parameter map: A mapping from ObjectMapper
  */
  public func mapping(map: Map) {
    originalRetailPrice <- map[kGDiscountPriceInfoOriginalRetailPriceKey]
    pricingTreatment <- map[kGDiscountPriceInfoPricingTreatmentKey]
    soldOneBay <- map[kGDiscountPriceInfoSoldOneBayKey]
    soldOffeBay <- map[kGDiscountPriceInfoSoldOffeBayKey]
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = originalRetailPrice { dictionary[kGDiscountPriceInfoOriginalRetailPriceKey] = value.dictionaryRepresentation() }
    if let value = pricingTreatment { dictionary[kGDiscountPriceInfoPricingTreatmentKey] = value }
    dictionary[kGDiscountPriceInfoSoldOneBayKey] = soldOneBay
    dictionary[kGDiscountPriceInfoSoldOffeBayKey] = soldOffeBay
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.originalRetailPrice = aDecoder.decodeObject(forKey: kGDiscountPriceInfoOriginalRetailPriceKey) as? GOriginalRetailPrice
    self.pricingTreatment = aDecoder.decodeObject(forKey: kGDiscountPriceInfoPricingTreatmentKey) as? String
    self.soldOneBay = aDecoder.decodeBool(forKey: kGDiscountPriceInfoSoldOneBayKey)
    self.soldOffeBay = aDecoder.decodeBool(forKey: kGDiscountPriceInfoSoldOffeBayKey)
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(originalRetailPrice, forKey: kGDiscountPriceInfoOriginalRetailPriceKey)
    aCoder.encode(pricingTreatment, forKey: kGDiscountPriceInfoPricingTreatmentKey)
    aCoder.encode(soldOneBay, forKey: kGDiscountPriceInfoSoldOneBayKey)
    aCoder.encode(soldOffeBay, forKey: kGDiscountPriceInfoSoldOffeBayKey)
  }

}
